<?php

namespace Fir\Pinecones\Hero38\Blocks;

use Log1x\AcfComposer\Block;
use StoutLogic\AcfBuilder\FieldsBuilder;
use Fir\Utils\GlobalFields as GlobalFields;
use function Roots\asset;

class Hero38 extends Block
{
    /**
     * The block name.
     *
     * @var string
     */
    public $name = 'Split Header';

    /**
     * The block view.
     *
     * @var string
     */
    public $view = 'Hero38.view';

    /**
     * The block description.
     *
     * @var string
     */
    public $description = '';

    /**
     * The block category.
     *
     * @var string
     */
    public $category = 'fir';

    /**
     * The block icon.
     *
     * @var string|array
     */
    public $icon = 'editor-ul';

    /**
     * The block keywords.
     *
     * @var array
     */
    public $keywords = ['hero38'];

    /**
     * The block post type allow list.
     *
     * @var array
     */
    public $post_types = [];

    /**
     * The parent block type allow list.
     *
     * @var array
     */
    public $parent = [];

    /**
     * The default block mode.
     *
     * @var string
     */
    public $mode = 'preview';

    /**
     * The default block alignment.
     *
     * @var string
     */
    public $align = '';

    /**
     * The default block text alignment.
     *
     * @var string
     */
    public $align_text = '';

    /**
     * The default block content alignment.
     *
     * @var string
     */
    public $align_content = '';

    /**
     * The supported block features.
     *
     * @var array
     */
    public $supports = [
        'align' => array('wide', 'full', 'other'),
        'align_text' => false,
        'align_content' => false,
        'mode' => false,
        'multiple' => true,
        'jsx' => true,
    ];

    /**
     * The block preview example data.
     *
     * @var array
     */
    public $defaults = [
        'content' => [
            'hero_image' => null,
            'hero_title' => null,
            'hero_text' =>  null,
            'hero_cta' => null,
            'list' => null
        ]
    ];

    /**
     * Data to be passed to the block before rendering.
     *
     * @return array
     */
    public function with()
    {
        $data = $this->parse(get_fields());
        $newData = [
        ];

        return array_merge($data, $newData);
    }

    private function parse($data)
    {

        $data['hero_image'] = ($data['content']['hero_image']) ?: $this->defaults['content']['hero_image'];
        $data['hero_title'] = $data['content']['hero_title'] ? $data['content']['hero_title'] : get_the_title();
        $data['hero_text'] = ($data['content']['hero_text']) ?: $this->defaults['content']['hero_text'];
        $data['hero_cta'] = ($data['content']['hero_cta']) ?: $this->defaults['content']['hero_cta'];
        $data['list'] = ($data['content']['list']) ?: $this->defaults['content']['list'];
        $data['flip'] = $data['options']['flip_horizontal'] ? 'hero-38--flip' : '';
        $data['text_align'] = 'hero-38--' . $data['options']['text_align'];
        $data['theme'] = 'fir--' . $data['options']['theme'];
        return $data;
    }

    /**
     * The block field group.
     *
     * @return array
     */
    public function fields()
    {

        $Hero38 = new FieldsBuilder('Split Header');

        $Hero38
            ->addGroup('content', [
                'label' => 'Split Header',
                'layout' => 'block'
            ])
                ->addImage('hero_image')
                ->addText('hero_title')
                ->addTextarea('hero_text')
                ->addRepeater('list', [
                    'layout' => 'block'
                ])
                    ->addText('list_text')
                ->endRepeater()
                ->addLink('hero_cta')
            ->endGroup()
            ->addGroup('options', [
                'label' => 'Options',
                'layout' => 'block'
            ])
            ->addFields(GlobalFields::getFields('flipHorizontal'))
            ->addFields(GlobalFields::getFields('textAlign'))
            ->addFields(GlobalFields::getFields('theme'))
            ->addFields(GlobalFields::getFields('hideComponent'))
            ->endGroup();

        return $Hero38->build();
    }

    /**
     * Assets to be enqueued when rendering the block.
     *
     * @return void
     */
    public function enqueue()
    {
        wp_enqueue_style('sage/app.css', asset('styles/fir/Pinecones/Hero38/style.css')->uri(), false, null);
    }
}
