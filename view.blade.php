<!-- Start hero38 -->
@if(!Fir\Utils\Helpers::isProduction())
<!-- Description : TBD -->
@endif
<section class="{{ $block->classes }}" is="fir-hero-38" id="{{ $pinecone_id ?? '' }}" data-title="{{ $pinecone_title ?? '' }}" data-observe-resizes>
  <div class="hero-38 {{ $theme }} {{ $text_align }} {{ $flip }}">
  <div class="hero-38__content">
        <h2>{{ $hero_title }}</h2>
        @if($hero_text)
        <p>{{ $hero_text }}</p>
        @endif
        @if($list)
        <ul>
          @foreach($list as $item)
          <li>{{ $item['list_text'] }}</li>\
          @endforeach
        </ul>
        @endif
        @if($hero_cta)
        <a class="hero-38__cta btn btn--full" href="{{ $hero_cta['url'] }}">{{ $hero_cta['title'] }}</a>
        @endif


      </div>
      <div class="hero-38__img-wrapper">
        <img class="hero-38__image" src="{{ $hero_image['url'] }}" alt="{{ get_the_title() }}">
      </div>
  </div>
</section>
<!-- End hero38 -->
