class Hero38 extends window.HTMLDivElement {

    constructor (...args) {
        const self = super(...args)
        self.init()
        return self
    }

    init () {
        this.props = this.getInitialProps()
        this.resolveElements()
      }

    getInitialProps () {
        let data = {}
        try {
            data = JSON.parse(this.querySelector('script[type="application/json"]').text())
        } catch (e) {}
        return data
    }

    resolveElements () {

    }

    connectedCallback () {
        this.initHero38()
    }

    initHero38 () {
        const { options } = this.props
        const config = {

        }

        // console.log("Init: hero38")
    }

}

window.customElements.define('fir-hero-38', Hero38, { extends: 'section' })
